package com.company.SpringRestAPI.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {
    @GetMapping(path = "/welcome")
    public String welcome()
    {
        return "Welcome to new Rest API using Gradle";
    }
}
